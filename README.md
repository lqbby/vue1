```html
<template>
  <div>
    <h1>翻转</h1>
    <p>{{ msg }}</p>
    <button @click="change">扭转乾坤</button>
  </div>
</template>

<script>
export default {
  data() {
    return {
      msg: 'sb',
    }
  },
  methods: {
    change() {
      this.msg = this.msg.split('').reverse().join('')
    },
  },
}
</script>

<style></style>
```
